import '../styles/index.scss';

import {tarifs} from './constants';
import positive from '../../public/img/posotove-answer.svg';
import negative from '../../public/img/negative-answer.svg';

let balance = 100;

console.log(tarifs);

const payment = {};
const payments = [];

let total = 0;
let totalTaxes = 0;
let totalWater = 0;
let totalInternet = 0;
let totalSecurity = 0;
let totalExchange = 0;

const getElById = (el) => document.getElementById(el);
const getElBySel = (el) => document.querySelector(el);

const companies = getElById('companies');
let centerTitle = getElBySel('.center__title');
let centerDesc = getElBySel('.center__desc');
const metersCounter = getElById('meters');

const centerForm = getElBySel('.center__form');
const previousInput = getElById('previous');
const currentInput = getElById('current');

const formSummeryList = getElBySel('.form__summary-list');

const totalList = getElBySel('.center .center__form .form__summary-list .list__total .price b');

const rightForm = getElBySel('.right__payments');
const rightPaymentsFields = getElBySel('.right__payments-fields');
const formData = getElBySel('.data__wrapper');
const transactionsList = getElBySel('.transactions__list');

const saveBtn = centerForm.querySelector('.btn');
const resetBtn = centerForm.querySelector('.btn-outline');
const paymentBtn = rightForm.querySelector('.btn');

const leftButtons = companies.querySelectorAll('div[data-id]');

leftButtons[1].classList.add('selected');

const div = document.createElement('div');

const selected = () => {
  const selectedEl = companies.querySelector('.selected');
  const idx = [...leftButtons].findIndex(el => el === selectedEl);
  switch (idx){
    case 0:
      payment.id = 'taxes';
      break;
    case 1:
      payment.id = 'water';
      break;
    case 2:
      payment.id = 'internet';
      break;
    case 3:
      payment.id = 'security';
      break;
    case 4:
      payment.id = 'exchange';
      break;
  }

};

const addError = (element) => {
  element.classList.add('error');
};
const removeError = (element) => {
  element.classList.remove('error');
};

const resetInputs = () => {
  payment.current = 0;
  payment.previous = 0;
  previousInput.value = '';
  currentInput.value = '';
};

const addErrorMessage =(div) => {
  div.innerText = 'Please, enter the positive number !!!';
  div.classList.add('error-warning');
  formData.appendChild(div) ;
};
const removeErrorMessage =(div) => {
  div.classList.remove('error-warning');
  div.innerText = '';
  previousInput.classList.remove('error');
  currentInput.classList.remove('error');
};

const switchCompHandler = (id, totalToPay) => {
  let innerText ='';
  switch (id){
    case 'taxes':
      innerText = 'Налоги';
      centerTitle.innerText = innerText;
      centerDesc.innerText = 'Оплата налогов';
      totalTaxes = totalToPay;
      break;
    case 'water':
      innerText = 'Холодная вода';
      centerTitle.innerText = innerText;
      centerDesc.innerText = 'Оплата холодного водоснабжения';
      totalWater = totalToPay;
      break;
    case 'internet':
      innerText = 'Интернет';
      centerTitle.innerText = innerText;
      centerDesc.innerText = 'Оплата интернета';
      totalInternet = totalToPay;
      break;
    case 'security':
      innerText = 'Охрана';
      centerTitle.innerText = innerText;
      centerDesc.innerText = 'Оплата охраны';
      totalSecurity = totalToPay;
      break;
    case 'exchange':
      innerText = 'Обмен валют';
      centerTitle.innerText = innerText;
      centerDesc.innerText = 'Оплата обмена валют';
      totalExchange = totalToPay;
      break;
  }
  return innerText;
};
const companiesClickHandler = () => {
  resetInputs();
  removeError(previousInput);
  removeError(currentInput);
  leftButtons.forEach(el => el.classList.remove('selected'));
  const selectedIndex = metersCounter.selectedIndex;
  payment.metersId = metersCounter.options[selectedIndex].text;
};

const createListEl = (metersId, total) =>{

  const li = document.createElement('li');
  li.classList.add('list__item');
  formSummeryList.insertAdjacentElement('afterbegin', li);

  const paragraph = document.createElement('p');
  li.appendChild(paragraph);

  const span1 = document.createElement('span');
  span1.classList.add('list__item-label');
  span1.innerText = metersId;
  paragraph.appendChild(span1);

  const span2 = document.createElement('span');
  span2.classList.add('price');
  span2.innerText = '$';
  paragraph.appendChild(span2);

  const price = document.createElement('b');
  price.innerText = total;
  span2.appendChild(price);
};

const createLiItemPositive = (text) => {
  let li = document.createElement('li');
  li.classList.add('list__item');
  li.textContent = `${text} : Успешно оплачено`;
  const img = document.createElement('IMG');
  img.classList.add('payment-img');
  img.src = positive;
  return li;
};
const createLiItemNegative = (text) => {
  let li = document.createElement('li');
  li.classList.add('list__item');
  li.classList.add('list__item-error');
  li.textContent = `${text}: Ошибка`;
  const img = document.createElement('IMG');
  img.classList.add('payment-img');
  img.src = negative;
  return li;
};

const createSavePayments = () => {

  const savePayments = rightPaymentsFields.querySelectorAll('.right__payments-field');
  const newArr = [];
  [...savePayments].forEach(el => newArr.push(el.innerText));
  new Set([...newArr]);

  const span = document.createElement('span');
  span.innerText = switchCompHandler(payment.id, total);

  if(!newArr.includes(span.innerText)){

  const paragraph = document.createElement('p');
  rightPaymentsFields.appendChild(paragraph);
  paragraph.classList.add('right__payments-field');

  const label = document.createElement('label');
  paragraph.appendChild(label);

  const input = document.createElement('input');
  input.type = 'checkbox';
  input.id = payment.id;
  input.value = switchCompHandler(payment.id, total);
  input.checked = true;

  label.appendChild(input);
  label.appendChild(span);
  }
};

const removeElFromList = () => {
  const li = formSummeryList.querySelectorAll('.list__item');
  const tot = formSummeryList.querySelector('.center .center__form .form__summary-list .list__total .price b');
  const newList = [...li].filter(el => !el.classList.contains('list__total'));
  newList.forEach(el => el.remove());
  tot.innerText = 0;
  payments.length = 0;
};

previousInput.oninput = ()  => {
  removeError(previousInput);
  if(Number(previousInput.value) <= 0|| isNaN(Number(previousInput.value))){
    addError(previousInput);
    payment.previous = 0;
    previousInput.value = '';
  }else{
    payment.previous = Number(previousInput.value);
  }
};
currentInput.oninput = ()  => {
  removeError(currentInput);
  if( (Number(currentInput.value) <= Number(previousInput.value)) || isNaN(Number(currentInput.value))){
    addError(currentInput);
    payment.current = 0;
  }else{
    payment.current = currentInput.value;
  }

};
metersCounter.onchange = (e) => {
  const selectedIndex = metersCounter.selectedIndex;
  payment.metersId = e.target[selectedIndex].text;
  resetInputs();
};

centerForm.onsubmit = (e) => {
  e.preventDefault();
  removeError(previousInput);
  removeError(currentInput);
};
companies.onclick = (e) => {
  companiesClickHandler();
  let el = e.target;
  if(el.parentNode.classList.contains('left__company')) {
    el = el.parentNode;
  }
  el.classList.add('selected');
  payment.id = el.dataset.id;
  switchCompHandler(payment.id, total);
  removeElFromList();
};

saveBtn.onclick = () => {
  selected();
  const selectedIndex = metersCounter.selectedIndex;
  payment.metersId = metersCounter.options[selectedIndex].text;
  payment.previous = Number(previousInput.value);

  if( (Number(currentInput.value) <= Number(previousInput.value)) || isNaN(Number(currentInput.value))){
    addError(currentInput);
    payment.current = 0;
  }else{
    payment.current = currentInput.value;
  }

  const {current, previous, id, metersId} = payment;
  const tarifConst = tarifs[id];
  const totalToPay = (current - previous) * tarifConst;
  if(totalToPay <= 0 || isNaN(totalToPay) || current <= 0 || previous <= 0) {
    addErrorMessage(div);
  }else {
    removeErrorMessage(div);
    payment.totalToPay = totalToPay;
    payments.push({...payment});
    createListEl(metersId, totalToPay);
  }

  const totalPayments = [];
  payments.forEach(el =>totalPayments.push(el.totalToPay));

  total = totalPayments.reduce((a, b) => a + b, 0);
  switchCompHandler(payment, total);
  totalList.innerText = total;

  createSavePayments();
  for (const i in payment) delete payment[i];
  resetInputs();
};

resetBtn.onclick = () => {
  removeElFromList();
  removeErrorMessage(div);
};

paymentBtn.onclick = (e) => {
  e.preventDefault();
  const selectedInputs = rightPaymentsFields.querySelectorAll('input');
  let rest = balance;
  selectedInputs.forEach(el => {
    switch (el.id){
      case 'taxes':
       rest -= totalTaxes;
        break;
      case 'water':
        rest -= totalWater;
        break;
      case 'internet':
        rest -= totalInternet;
        break;
      case 'security':
        rest -= totalSecurity;
        break;
      case 'exchange':
        rest -= totalExchange;
        break;
    }
    if (rest >= 0) {
      transactionsList.appendChild(createLiItemPositive(el.value));
      removeElFromList();
      const selectedInputsPaid = rightPaymentsFields.querySelectorAll('p');
      selectedInputsPaid.forEach(el => el.remove());
    } else{
      transactionsList.appendChild(createLiItemNegative(el.value));
    }
     });
};
